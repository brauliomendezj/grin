package com.braulio.example.grin.bindings

import android.databinding.BindingAdapter
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.braulio.example.grin.screens.get_devices.view.adapter.GetBluetoothDevicesAdapter
import com.braulio.example.grin.screens.get_devices.viewmodel.BluetoothDeviceItemViewModel
import com.braulio.example.grin.screens.show_devices.view.adapter.BluetoothDevicesAdapter
import com.braulio.example.grin.screens.show_devices.viewmodel.BluetoothItemViewModel

object Bindings {

    @BindingAdapter("set_items")
    @JvmStatic
    fun configRecyclerView(recyclerView: RecyclerView, items: List<BluetoothItemViewModel>?) {
        val adapter = BluetoothDevicesAdapter()
        items?.let { adapter.items = it }
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = adapter
    }

    @BindingAdapter("set_items_bluetooth")
    @JvmStatic
    fun configBluetoothRecyclerView(recyclerView: RecyclerView, items: List<BluetoothDeviceItemViewModel>?) {
        val adapter = GetBluetoothDevicesAdapter()
        items?.let { adapter.items = it }
        recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
        recyclerView.adapter = adapter
    }

    @BindingAdapter("set_visibility")
    @JvmStatic
    fun setVisibility(view: View, isVisible: Boolean) {
        view.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

}
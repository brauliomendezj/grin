package com.braulio.example.grin.bluetooth

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.Intent
import com.braulio.example.grin.di.scope.ApplicationScope
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject


@ApplicationScope
class BluetoothController @Inject constructor(private val context: Context) {

    private val bluetoothAdapter: BluetoothAdapter? by lazy { (context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager).adapter }

    private val bluetoothLeScanner: BluetoothLeScanner? by lazy { bluetoothAdapter?.bluetoothLeScanner }

    private val devicesSubject: BehaviorSubject<BluetoothDevice> = BehaviorSubject.create()

    private val bluetoothCallBack: ScanCallback by lazy {
        object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult?) {
                result?.let { devicesSubject.onNext(it.device) }
                super.onScanResult(callbackType, result)
            }
        }
    }

    fun isBluetoothEnabled(): Boolean {
        return bluetoothAdapter?.isEnabled ?: false
    }

    fun isBluetoothAvailable(): Boolean {
        return !(bluetoothAdapter == null || bluetoothAdapter?.address.isNullOrEmpty())
    }

    fun enableBluetooth(activity: Activity, requestCode: Int) {
        if (!isBluetoothEnabled()) {
            activity.startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), requestCode)
        }
    }

    fun startBluetooth() {
        val settings = ScanSettings.Builder().setScanMode(ScanSettings.SCAN_MODE_LOW_POWER).build()
        bluetoothLeScanner?.startScan(null, settings, bluetoothCallBack)
    }

    fun stopBluetooth() {
        bluetoothLeScanner?.stopScan(bluetoothCallBack)
    }

    fun devicesObservable(): Observable<BluetoothDevice> = devicesSubject.hide()

}
package com.braulio.example.grin.screens.get_devices.view.adapter

import android.support.v7.widget.RecyclerView
import com.braulio.example.grin.databinding.ItemBluetoothDevicePhoneBinding
import com.braulio.example.grin.screens.get_devices.viewmodel.BluetoothDeviceItemViewModel

class GetBluetoothDevicesViewHolder(private val binding: ItemBluetoothDevicePhoneBinding): RecyclerView.ViewHolder(binding.root) {

    fun bind(bluetoothDeviceItemViewModel: BluetoothDeviceItemViewModel) {
        binding.viewModel = bluetoothDeviceItemViewModel
        binding.executePendingBindings()
    }

}
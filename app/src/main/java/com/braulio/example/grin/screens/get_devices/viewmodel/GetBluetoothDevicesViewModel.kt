package com.braulio.example.grin.screens.get_devices.viewmodel

import android.bluetooth.BluetoothDevice
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.braulio.example.grin.R
import com.braulio.example.grin.api.controller.BluetoothDeviceController
import com.braulio.example.grin.app.GrinApplication
import com.braulio.example.grin.bluetooth.BluetoothController
import com.braulio.example.grin.di.scope.ScreenScope
import com.braulio.example.grin.extensions.apiError
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@ScreenScope
class GetBluetoothDevicesViewModel @Inject constructor(private val bluetoothDeviceController: BluetoothDeviceController,
                                                       val bluetoothController: BluetoothController) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val enableBluetoothSubject: PublishSubject<Unit> = PublishSubject.create()
    private val startScanningDevicesSubject: PublishSubject<Unit> = PublishSubject.create()
    private val showErrorSubject: PublishSubject<String> = PublishSubject.create()
    private val showMoreDevicesSubject: PublishSubject<Unit> = PublishSubject.create()

    val isLoading: ObservableBoolean = ObservableBoolean()
    val isBluetoothScanning: ObservableBoolean = ObservableBoolean()
    val isBluetoothEnabled: ObservableBoolean = ObservableBoolean()

    private val bluetoothDevices: MutableSet<BluetoothDevice> = mutableSetOf()

    var items: ObservableField<List<BluetoothDeviceItemViewModel>> = ObservableField()

    private val onClickBluetoothDevice: (BluetoothDeviceItemViewModel) -> Unit = {
        sendBluetoothDevice(it.name.get()?: "", it.address.get()?: "", "-20db")
    }

    fun bind() {
        isBluetoothEnabled.set(bluetoothController.isBluetoothEnabled())

        compositeDisposable.addAll(bluetoothController.devicesObservable().subscribe {
            bluetoothDevices.add(it)
            items.set(bluetoothDevices.map { BluetoothDeviceItemViewModel(it, onClickBluetoothDevice) })
        })
    }

    fun dispose() {
        compositeDisposable.clear()
    }

    private fun sendBluetoothDevice(name: String, address: String, strength: String) {
        compositeDisposable.add(bluetoothDeviceController.sendBluetoothDevice(name, address, strength)
            .doOnSubscribe { isLoading.set(true) }
            .doFinally { isLoading.set(false) }
            .subscribe({ }, { onErrorRequest(it) }))
    }

    private fun onErrorRequest(throwable: Throwable) {
        showErrorSubject.onNext(throwable.apiError())
    }

    fun startDiscovery() {
        if (!bluetoothController.isBluetoothAvailable()) {
            showErrorSubject.onNext(GrinApplication.get().getString(R.string.bluetooth_unavailable))
        } else {
            isLoading.set(true)
            isBluetoothScanning.set(true)
            bluetoothController.startBluetooth()
        }
    }

    fun stopDiscovery() {
        isLoading.set(false)
        isBluetoothScanning.set(false)
        bluetoothController.stopBluetooth()
    }

    fun enableBluetooth() {
        enableBluetoothSubject.onNext(Unit)
    }

    fun startScanningDevices() {
        startScanningDevicesSubject.onNext(Unit)
    }

    fun showMoreDevices() {
        showMoreDevicesSubject.onNext(Unit)
    }

    fun enableBluetoothObservable(): Observable<Unit> = enableBluetoothSubject.hide()
    fun startScanningDevicesObservable(): Observable<Unit> = startScanningDevicesSubject.hide()
    fun showMoreDevicesObservable(): Observable<Unit> = showMoreDevicesSubject.hide()
    fun showErrorObservable(): Observable<String> = showErrorSubject.hide()

}
package com.braulio.example.grin.screens.get_devices.view.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import com.braulio.example.grin.R
import com.braulio.example.grin.databinding.ActivityGetBluetoothDevicesBinding
import com.braulio.example.grin.di.Dagger
import com.braulio.example.grin.screens.get_devices.viewmodel.GetBluetoothDevicesViewModel
import com.braulio.example.grin.screens.show_devices.view.activity.BluetoothDevicesActivity
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class GetBluetoothDevicesActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: GetBluetoothDevicesViewModel

    private val binding: ActivityGetBluetoothDevicesBinding by lazy {
        DataBindingUtil.setContentView<ActivityGetBluetoothDevicesBinding>(this, R.layout.activity_get_bluetooth_devices)
    }

    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        Dagger.getOrBuildScreenComponent().inject(this)
        super.onCreate(savedInstanceState)
        setUpToolbar()
        binding.viewModel = viewModel
    }

    override fun onResume() {
       super.onResume()
       bindViewModel()
    }

    override fun onPause() {
        viewModel.dispose()
        compositeDisposable.clear()
        super.onPause()
    }

    override fun onDestroy() {
        if (isFinishing) {
            Dagger.releaseScreenComponent()
        }
        super.onDestroy()
    }

    private fun bindViewModel() {
        viewModel.bind()
        compositeDisposable.addAll(viewModel.showMoreDevicesObservable().subscribe { startActivity(Intent(this, BluetoothDevicesActivity::class.java)) },
            viewModel.startScanningDevicesObservable().subscribe { checkPermissions() },
            viewModel.enableBluetoothObservable().subscribe { viewModel.bluetoothController.enableBluetooth(this, ENABLE_BLUETOOTH_CODE) },
            viewModel.showErrorObservable().subscribe { Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show() })
    }

    private fun setUpToolbar() {
        setSupportActionBar(binding.toolbar)
        title = getString(R.string.bluetooth_get_title)
    }

    private fun checkPermissions() {
        var permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        permissionCheck += ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
        if (permissionCheck != 0) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), PERMISSION_REQUEST_CODE)
        } else {
            viewModel.startDiscovery()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    viewModel.startDiscovery()
                } else {
                    Snackbar.make(binding.root, getString(R.string.error_permission_denied), Snackbar.LENGTH_LONG).show()
                }
                return
            }
        }
    }

    companion object {
        private const val PERMISSION_REQUEST_CODE = 1001
        private const val ENABLE_BLUETOOTH_CODE = 1002
    }

}
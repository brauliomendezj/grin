package com.braulio.example.grin.screens.show_devices.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.braulio.example.grin.R
import com.braulio.example.grin.databinding.ItemBluetoothDeviceWsBinding
import com.braulio.example.grin.screens.show_devices.viewmodel.BluetoothItemViewModel

class BluetoothDevicesAdapter : RecyclerView.Adapter<BluetoothDevicesViewHolder>() {

    var items: List<BluetoothItemViewModel> = emptyList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, parent: Int): BluetoothDevicesViewHolder {
        val binding = DataBindingUtil.inflate<ItemBluetoothDeviceWsBinding>(LayoutInflater.from(viewGroup.context), R.layout.item_bluetooth_device_ws, viewGroup, false)
        return BluetoothDevicesViewHolder(binding)
    }

    override fun onBindViewHolder(bluetoothDevicesViewHolder: BluetoothDevicesViewHolder, position: Int) = bluetoothDevicesViewHolder.bind(items[position])

    override fun getItemCount(): Int = items.size

}
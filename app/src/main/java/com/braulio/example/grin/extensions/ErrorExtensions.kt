package com.braulio.example.grin.extensions

import com.braulio.example.grin.R
import com.braulio.example.grin.app.GrinApplication
import retrofit2.HttpException
import java.net.ConnectException
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.UnknownHostException

fun Throwable.apiError(): String {
    return when (this) {
        is UnknownHostException, is ConnectException -> GrinApplication.get().getString(R.string.error_connection)
        is SocketTimeoutException -> GrinApplication.get().getString(R.string.error_timeout)
        is HttpException -> this.getErrorMessage()
        else -> GrinApplication.get().getString(R.string.error_default)
    }
}

fun HttpException.getErrorMessage(): String {
    return when (this.code()) {
        HttpURLConnection.HTTP_INTERNAL_ERROR -> GrinApplication.get().getString(R.string.error_server_internal)
        HttpURLConnection.HTTP_NOT_FOUND -> GrinApplication.get().getString(R.string.error_not_found)
        HttpURLConnection.HTTP_UNAVAILABLE -> GrinApplication.get().getString(R.string.error_unavailable)
        else -> GrinApplication.get().getString(R.string.error_default)
    }
}


package com.braulio.example.grin.screens.show_devices.view.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.braulio.example.grin.R
import com.braulio.example.grin.databinding.ActivityBluetoothDevicesBinding
import com.braulio.example.grin.di.Dagger
import com.braulio.example.grin.screens.show_devices.viewmodel.BluetoothDevicesViewModel
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class BluetoothDevicesActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModel: BluetoothDevicesViewModel

    private val binding: ActivityBluetoothDevicesBinding by lazy {
        DataBindingUtil.setContentView<ActivityBluetoothDevicesBinding>(this, R.layout.activity_bluetooth_devices)
    }

    private val compositeDisposable: CompositeDisposable by lazy { CompositeDisposable() }

    override fun onCreate(savedInstanceState: Bundle?) {
        Dagger.getOrBuildScreenComponent().inject(this)
        super.onCreate(savedInstanceState)
        setUpToolbar()
        binding.viewModel = viewModel
        bindViewModel()
    }

    override fun onDestroy() {
        if (isFinishing) {
            Dagger.releaseScreenComponent()
        }
        viewModel.dispose()
        compositeDisposable.clear()
        super.onDestroy()
    }

    private fun bindViewModel() {
        viewModel.bind()
        compositeDisposable.addAll(viewModel.showErrorObservable().subscribe { Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show() })
    }

    private fun setUpToolbar() {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = getString(R.string.bluetooth_devices_title)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

}

package com.braulio.example.grin.screens.show_devices.viewmodel

import android.databinding.ObservableField
import com.braulio.example.grin.R
import com.braulio.example.grin.app.GrinApplication
import com.braulio.example.grin.screens.show_devices.model.BluetoothDevice

class BluetoothItemViewModel(bluetoothDevice: BluetoothDevice) {

    val id: ObservableField<String> = ObservableField("")
    val name: ObservableField<String> = ObservableField("")
    val address: ObservableField<String> = ObservableField("")
    val strength: ObservableField<String> = ObservableField("")
    val createdAt: ObservableField<String> = ObservableField("")

    init {
        id.set(setItemFormat(R.string.bluetooth_item_id, bluetoothDevice.id))
        name.set(setItemFormat(R.string.bluetooth_item_name, bluetoothDevice.name))
        address.set(setItemFormat(R.string.bluetooth_item_address, bluetoothDevice.address))
        strength.set(setItemFormat(R.string.bluetooth_item_strength, bluetoothDevice.strength))
        createdAt.set(setItemFormat(R.string.bluetooth_item_createdAt, bluetoothDevice.createdAt))
    }

    private fun setItemFormat(prefix: Int, property: String?) = "${GrinApplication.get().getString(prefix)} $property"

}
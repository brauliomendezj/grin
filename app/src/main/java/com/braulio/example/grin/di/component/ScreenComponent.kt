package com.braulio.example.grin.di.component

import com.braulio.example.grin.di.scope.ScreenScope
import com.braulio.example.grin.screens.get_devices.view.activity.GetBluetoothDevicesActivity
import com.braulio.example.grin.screens.show_devices.view.activity.BluetoothDevicesActivity
import dagger.Component

@ScreenScope
@Component(dependencies = [(ApplicationComponent::class)])
interface ScreenComponent {

    /**
     * Injected Activities
     */
    fun inject(bluetoothDevicesActivity: BluetoothDevicesActivity)

    fun inject(getBluetoothDevicesActivity: GetBluetoothDevicesActivity)

}
package com.braulio.example.grin.screens.show_devices.view.adapter

import android.support.v7.widget.RecyclerView
import com.braulio.example.grin.databinding.ItemBluetoothDeviceWsBinding
import com.braulio.example.grin.screens.show_devices.viewmodel.BluetoothItemViewModel

class BluetoothDevicesViewHolder(private val binding: ItemBluetoothDeviceWsBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(bluetoothItemViewModel: BluetoothItemViewModel) {
        binding.viewModel = bluetoothItemViewModel
        binding.executePendingBindings()
    }

}
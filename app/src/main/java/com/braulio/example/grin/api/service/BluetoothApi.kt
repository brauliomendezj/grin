package com.braulio.example.grin.api.service

import com.braulio.example.grin.screens.show_devices.model.BluetoothDevice
import com.braulio.example.grin.screens.show_devices.model.BluetoothDeviceBody
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface BluetoothApi {

    @GET("devices/")
    fun getBluetoothDevices(): Single<List<BluetoothDevice>>

    @POST("add")
    fun sendBluetoothDevice(@Body bluetoothDeviceBody: BluetoothDeviceBody): Single<BluetoothDevice>

}
package com.braulio.example.grin.screens.get_devices.view.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.braulio.example.grin.R
import com.braulio.example.grin.databinding.ItemBluetoothDevicePhoneBinding
import com.braulio.example.grin.screens.get_devices.viewmodel.BluetoothDeviceItemViewModel

class GetBluetoothDevicesAdapter: RecyclerView.Adapter<GetBluetoothDevicesViewHolder>() {

    var items: List<BluetoothDeviceItemViewModel> = emptyList()

    override fun onCreateViewHolder(viewGroup: ViewGroup, parent: Int): GetBluetoothDevicesViewHolder {
        val binding = DataBindingUtil.inflate<ItemBluetoothDevicePhoneBinding>(LayoutInflater.from(viewGroup.context), R.layout.item_bluetooth_device_phone, viewGroup, false)
        return GetBluetoothDevicesViewHolder(binding)
    }

    override fun onBindViewHolder(viewHolder: GetBluetoothDevicesViewHolder, position: Int) = viewHolder.bind(items[position])

    override fun getItemCount(): Int = items.size

}
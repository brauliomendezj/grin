package com.braulio.example.grin.di

import com.braulio.example.grin.app.GrinApplication
import com.braulio.example.grin.di.component.ApplicationComponent
import com.braulio.example.grin.di.component.DaggerApplicationComponent
import com.braulio.example.grin.di.component.DaggerScreenComponent
import com.braulio.example.grin.di.component.ScreenComponent
import com.braulio.example.grin.di.module.AppModule

object Dagger {

    lateinit var appComponent: ApplicationComponent
        private set

    private var screenComponent: ScreenComponent? = null

    fun buildAppComponent(rappiApplication: GrinApplication) {
        appComponent = DaggerApplicationComponent.builder()
            .appModule(AppModule(rappiApplication))
            .build()
    }

    fun getOrBuildScreenComponent(): ScreenComponent {
        return screenComponent.let {
            if (it == null) {
                val component = DaggerScreenComponent.builder().applicationComponent(appComponent).build()
                screenComponent = component
                component
            } else {
                it
            }
        }
    }

    fun releaseScreenComponent() {
        screenComponent = null
    }

}
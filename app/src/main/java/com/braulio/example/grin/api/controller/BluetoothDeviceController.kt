package com.braulio.example.grin.api.controller

import com.braulio.example.grin.api.service.BluetoothApi
import com.braulio.example.grin.di.scope.ApplicationScope
import com.braulio.example.grin.screens.show_devices.model.BluetoothDevice
import com.braulio.example.grin.screens.show_devices.model.BluetoothDeviceBody
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

@ApplicationScope
class BluetoothDeviceController @Inject constructor(private val bluetoothApi: BluetoothApi) {

    fun getBluetoothDevices(): Single<List<BluetoothDevice>> {
        return bluetoothApi.getBluetoothDevices()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun sendBluetoothDevice(name: String, address: String, strength: String): Single<BluetoothDevice> {
        return bluetoothApi.sendBluetoothDevice(BluetoothDeviceBody(name, address, strength))
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
package com.braulio.example.grin.di.scope

import javax.inject.Scope

@Scope
@Retention
annotation class ApplicationScope
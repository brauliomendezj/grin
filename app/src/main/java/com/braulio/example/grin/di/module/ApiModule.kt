package com.braulio.example.grin.di.module

import com.braulio.example.grin.R
import com.braulio.example.grin.api.service.BluetoothApi
import com.braulio.example.grin.app.GrinApplication
import com.braulio.example.grin.di.scope.ApplicationScope
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule {

    @Provides
    @ApplicationScope
    fun bluetoothApi(retrofit: Retrofit): BluetoothApi {
        return retrofit.create(BluetoothApi::class.java)
    }

    @Provides
    @ApplicationScope
    fun retrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(GrinApplication.get().getString(R.string.url_base))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
    }

    @Provides
    @ApplicationScope
    fun gson(): Gson {
        return GsonBuilder()
            .create()
    }

}
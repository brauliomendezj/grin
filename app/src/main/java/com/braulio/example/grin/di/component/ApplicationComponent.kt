package com.braulio.example.grin.di.component

import com.braulio.example.grin.api.controller.BluetoothDeviceController
import com.braulio.example.grin.bluetooth.BluetoothController
import com.braulio.example.grin.di.module.ApiModule
import com.braulio.example.grin.di.module.AppModule
import com.braulio.example.grin.di.scope.ApplicationScope
import com.google.gson.Gson
import dagger.Component

@ApplicationScope
@Component(modules = [(AppModule::class), (ApiModule::class)])
interface ApplicationComponent {

    /**
     * Expose general dependencies to subcomponents
     */
    fun gson(): Gson

    fun bluetoothController(): BluetoothController

    /**
     * Expose controllers to subcomponents
     */
    fun bluetoothDevicesController(): BluetoothDeviceController

}
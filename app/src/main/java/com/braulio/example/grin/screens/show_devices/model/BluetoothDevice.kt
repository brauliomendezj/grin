package com.braulio.example.grin.screens.show_devices.model

import com.google.gson.annotations.SerializedName

data class BluetoothDevice(@SerializedName("_id") val id: String? = "",
                           @SerializedName("name") val name: String? = "",
                           @SerializedName("address") val address: String? = "",
                           @SerializedName("strength") val strength: String? = "",
                           @SerializedName("created_at") val createdAt: String? = "")
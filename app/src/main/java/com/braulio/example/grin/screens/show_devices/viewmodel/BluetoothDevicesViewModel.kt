package com.braulio.example.grin.screens.show_devices.viewmodel

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import com.braulio.example.grin.api.controller.BluetoothDeviceController
import com.braulio.example.grin.di.scope.ScreenScope
import com.braulio.example.grin.extensions.apiError
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

@ScreenScope
class BluetoothDevicesViewModel @Inject constructor(private val bluetoothDeviceController: BluetoothDeviceController) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    private val showErrorSubject: PublishSubject<String> = PublishSubject.create()

    val isLoading: ObservableBoolean = ObservableBoolean()

    var items: ObservableField<List<BluetoothItemViewModel>> = ObservableField()

    fun bind() {
        getBluetoothDevices()
    }

    fun dispose() {
        compositeDisposable.clear()
    }

    private fun getBluetoothDevices() {
        compositeDisposable.add(bluetoothDeviceController.getBluetoothDevices()
            .doOnSubscribe { isLoading.set(true) }
            .doFinally { isLoading.set(false) }
            .subscribe({ items.set(it.map { BluetoothItemViewModel(it) }) }, { onErrorRequest(it) }))
    }

    private fun onErrorRequest(throwable: Throwable) {
        showErrorSubject.onNext(throwable.apiError())
    }

    fun sortItems() {
        val listSorted = items.get()?.sortedByDescending { it.createdAt.get() }
        items.set(listSorted)
    }

    fun showErrorObservable(): Observable<String> = showErrorSubject.hide()

}
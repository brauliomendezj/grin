package com.braulio.example.grin.screens.get_devices.viewmodel

import android.bluetooth.BluetoothDevice
import android.databinding.ObservableField
import com.braulio.example.grin.R
import com.braulio.example.grin.app.GrinApplication

class BluetoothDeviceItemViewModel(bluetoothDevice: BluetoothDevice, private val onClickDevice: (BluetoothDeviceItemViewModel) -> Unit) {

    val name: ObservableField<String> = ObservableField("")
    val address: ObservableField<String> = ObservableField("")

    init {
        name.set(bluetoothDevice.name ?: GrinApplication.get().getString(R.string.bluetooth_item_no_name))
        address.set(bluetoothDevice.address)
    }

    fun onClick() {
        onClickDevice.invoke(this)
    }

}
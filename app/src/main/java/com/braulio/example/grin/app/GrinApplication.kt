package com.braulio.example.grin.app

import android.app.Application
import com.braulio.example.grin.di.Dagger

class GrinApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        Dagger.buildAppComponent(this)
    }

    companion object {
        lateinit var instance: GrinApplication

        fun get(): GrinApplication {
            return instance
        }
    }

}
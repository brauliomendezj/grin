package com.braulio.example.grin.di.module

import android.content.Context
import com.braulio.example.grin.app.GrinApplication
import com.braulio.example.grin.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val app: GrinApplication) {

    @Provides
    @ApplicationScope
    fun appContext(): Context = app

}
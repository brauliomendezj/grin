﻿# Grin Test

# Architecture

In this project I used the architecture **Model View View Model.**
With this architecture is easy to organize the code, set more legibility and separate the layers of the application.

### Model

Holds the business logic. Exposes data from different sources (APIs, Cache, Persistence).

### ViewModel

Applies the UI logic and exposes the data for the View to consume.

Cannot reference the view directly, use data binding instead. Should be platform independent, this means no Android SDK references.

### View

User Interface: Activity, Fragment, View.

Informs the ViewModel about user’s actions and reflects current data state.

### Controller

Manage all logic to work with requests to APIs, Data Bases or some external service. 

# Libraries
I used this libraries to build the project.

> **Retrofit, OkHttp, Gson:** To realize the HTTP Requests and serealized the data. 

> **Rx:** To manage with data streams and manage their work threads. 

> **Dagger:** To set the dependency injection in the project. 


# Task Order

These were the order of the tasks that I realized to achieve the goal.

1. Set up the project.
2. Set libraries needed.
3. Set architecture.
4. Set Dagger config.
5. Get the devices from WS
6. Get the devices from Bluetooth.
7. Send request to save a device in WS


